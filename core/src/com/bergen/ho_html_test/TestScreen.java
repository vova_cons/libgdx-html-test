package com.bergen.ho_html_test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.bergen.ho_html_test.tests.ColorGradingTestView;
import com.bergen.ho_html_test.tests.CropShaderTestView;
import com.bergen.ho_html_test.tests.GrayShaderTestView;
import com.bergen.ho_html_test.tests.LazyLoadingAssetsTestView;
import com.bergen.ho_html_test.tests.LocalizationTestView;
import com.bergen.ho_html_test.tests.ParticlesTestView;
import com.bergen.ho_html_test.tests.SoundsTestView;
import com.bergen.ho_html_test.tests.SpinTestView;
import com.bergen.ho_html_test.tests.TestView;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by anbu on 21.10.19.
 **/
public class TestScreen extends BaseScreen {
    private List<TestView> groups = new LinkedList<TestView>();
    private int index = 0;

    @Override
    public void start() {
        groups.add(new LocalizationTestView());
        groups.add(new ParticlesTestView());
        groups.add(new GrayShaderTestView());
        groups.add(new CropShaderTestView());
        groups.add(new ColorGradingTestView());
        groups.add(new SpinTestView());
        groups.add(new SoundsTestView());
        groups.add(new LazyLoadingAssetsTestView());
        for(Group group : groups) {
            group.setVisible(false);
            this.addActor(group);
        }
        updateGroups();
    }

    @Override
    public void update(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
            if (index > 0) {
                index--;
                updateGroups();
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
            if (index < groups.size()-1) {
                index++;
                updateGroups();
            }
        }
    }

    private void updateGroups() {
        for(TestView group : groups) {
            group.hide();
        }
        groups.get(index).show();
    }
}
