package com.bergen.ho_html_test;

import com.badlogic.gdx.Game;
//import com.bergen.ho_html_test.fonts.FontsService;
//import com.bergen.ho_html_test.fonts.FontsServiceV1;

public class HoHtmlTest extends Game {
	public static final float SCENE_WIDE_WIDTH_IPHONEX = 3328f;
	public static final float SCENE_WIDE_WIDTH = 2736f;
	public static final float SCENE_WIDTH = 2048f;
	public static final float SCENE_HEIGHT = 1536f;
    private BaseScreen nextScreen = null;
//	public static FontsService fontsService;

	@Override
	public void create() {
		Env.game = this;
//		fontsService = new FontsServiceV1();
		setScreen(new LoadingScreen());
	}

	public void changeScreen(BaseScreen screen) {
	    this.nextScreen = screen;
    }

    @Override
    public void render() {
        super.render();
        if (nextScreen != null) {
            setScreen(nextScreen);
            nextScreen = null;
        }
    }
}
