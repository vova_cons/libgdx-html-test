package com.bergen.ho_html_test.utils;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.Group;

/**
 * Created by anbu on 31.07.17.
 */

public class ShaderedGroup extends Group {
    protected boolean isShaderEnabled = false;
    protected ShaderProgram shaderProgram;

    public ShaderedGroup(ShaderProgram shaderProgram) {
        this.shaderProgram = shaderProgram;
    }

    public ShaderProgram getShaderProgram() {
        return shaderProgram;
    }

    public void setShaderEnable(boolean state){
        this.isShaderEnabled = state;
    }

    public boolean isShaderEnabled() {
        return isShaderEnabled;
    }

    @Override
    protected void drawChildren(Batch batch, float parentAlpha) {
        if (isShaderEnabled) {
            ShaderProgram oldShader = batch.getShader();
            batch.setShader(shaderProgram);
            super.drawChildren(batch, parentAlpha);
            batch.setShader(oldShader);
        } else {
            super.drawChildren(batch, parentAlpha);
        }

    }
}
