package com.bergen.ho_html_test.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.bergen.ho_html_test.BaseScreen;

/**
 * Created by anbu on 12.08.19.
 **/
public class CropShaderedGroup extends Group {
    private ShaderProgram shaderProgram;
    private FrameBuffer originalBuffer;
    private TextureRegion baseTexture;
    private FrameBuffer maskBuffer;
    private Actor originalActor;
    private Actor maskActor;
    private float x;
    private float y;
    private Vector2 zeroPosition;

    public CropShaderedGroup() {
        this((int)BaseScreen.WIDTH, (int)BaseScreen.HEIGHT);
    }

    public CropShaderedGroup(int width, int height) {
        shaderProgram = new ShaderProgram(Gdx.files.internal("shaders/crop.vert"), Gdx.files.internal("shaders/crop.frag"));
        if (!shaderProgram.isCompiled()) {
            System.err.println("Crop shader error: " + shaderProgram.getLog());
        }
        this.originalBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, true);
        this.maskBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, true);
    }

    public void setOriginalActor(Actor originalActor) {
        this.originalActor = originalActor;
        this.addActor(originalActor);
    }

    public void setMaskActor(Actor maskActor) {
        this.maskActor = maskActor;
        this.addActor(maskActor);
    }

    @Override
    protected void drawChildren(Batch batch, float parentAlpha) {
        drawContent(batch, parentAlpha);
        drawMask(batch, parentAlpha);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        resetTransform(batch);
        super.draw(batch, parentAlpha);
        settingShader();
        workShader(batch, parentAlpha);
    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {}

    @Override
    protected void drawDebugChildren(ShapeRenderer shapes) {}

    private void drawContent(Batch batch, float parentAlpha) {
        maskActor.remove();
        originalBuffer.begin();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
        originalActor.draw(batch, parentAlpha);
        batch.flush();
        originalBuffer.end();
        if (baseTexture == null) {
            baseTexture = new TextureRegion(originalBuffer.getColorBufferTexture());
            baseTexture.flip(false, true);
        } else {
            baseTexture.setTexture(originalBuffer.getColorBufferTexture());
        }
        this.addActor(maskActor);
    }

    private void drawMask(Batch batch, float parentAlpha) {
        originalActor.remove();
        maskBuffer.begin();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
        maskActor.draw(batch, parentAlpha);
        batch.flush();
        maskBuffer.end();
        this.addActor(originalActor);
    }

    private void settingShader() {
        shaderProgram.begin();
        maskBuffer.getColorBufferTexture().bind(1);
        baseTexture.getTexture().bind(0);
        shaderProgram.setUniformi("u_mask", 1);
        shaderProgram.setUniformi("u_texture", 0);
        shaderProgram.end();
    }

    private void workShader(Batch batch, float parentAlpha) {
        ShaderProgram oldShader = batch.getShader();
        batch.setShader(shaderProgram);
        batch.draw(baseTexture, 0, 0);
        batch.flush();
        batch.setShader(oldShader);
    }
}
