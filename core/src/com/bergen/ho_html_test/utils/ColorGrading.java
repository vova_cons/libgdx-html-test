package com.bergen.ho_html_test.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Disposable;
import com.bergen.ho_html_test.BaseScreen;

/**
 * Created by anbu on 23.10.19.
 **/
public class ColorGrading extends Group implements Disposable {
    protected ShaderProgram colorGradingShader;
    protected Texture palette;
    protected Texture nightPalette;
    protected FrameBuffer gradingBuffer;
    protected TextureRegion baseTexture;
    protected float progress = 1.0f;
    protected float lutSize = 16.0f;

    protected void prepare() {
        if (gradingBuffer == null) {
            gradingBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, (int) BaseScreen.WIDTH, (int) BaseScreen.HEIGHT, true);
            colorGradingShader = new ShaderProgram(Gdx.files.internal("shaders/color_grading.vert"), Gdx.files.internal("shaders/color_grading.frag"));
            if (!colorGradingShader.isCompiled()) {
                System.err.println(colorGradingShader.getLog());
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        prepare();
        ShaderProgram prevShader = batch.getShader();
        batch.setShader(null);
        gradingBuffer.begin();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
        super.draw(batch, parentAlpha);
        batch.flush();
        gradingBuffer.end();
        if (baseTexture == null) {
            baseTexture = new TextureRegion(gradingBuffer.getColorBufferTexture());
            baseTexture.flip(false, true);
        } else {
            baseTexture.setTexture(gradingBuffer.getColorBufferTexture());
        }
        settingColorGradeShader();
        batch.setShader(colorGradingShader);
        batch.draw(baseTexture,0,0);
        batch.flush();
        batch.setShader(prevShader);
    }

    protected void settingColorGradeShader() {
        colorGradingShader.begin();
        palette.bind(6);
        nightPalette.bind(7);
        colorGradingShader.setUniformi("u_palette", 6);
        colorGradingShader.setUniformi("u_paletteNight", 7);
        baseTexture.getTexture().bind(0);
        colorGradingShader.setUniformi("u_texture", 0);
        colorGradingShader.setUniformf("u_lutSize", lutSize);
        colorGradingShader.setUniformf("u_progress", progress);
        colorGradingShader.end();
    }

    public void setPalette(Texture palette) {
        this.palette = palette;
    }

    public void setPaletteNight(Texture palette) {
        this.nightPalette = palette;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    @Override
    public void dispose() {
        colorGradingShader.dispose();
        palette.dispose();
        gradingBuffer.dispose();
    }
}
