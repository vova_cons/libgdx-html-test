package com.bergen.ho_html_test.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.bergen.ho_html_test.Env;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonJson;
import com.esotericsoftware.spine.SkeletonRenderer;
import com.esotericsoftware.spine.Skin;

import java.util.Objects;

/**
 * Created by anbu on 07.02.19.
 **/
public class SpineActor extends Group implements Disposable {
    private String id;
    private SkeletonXmlParser xmlConfig;
    private TextureAtlas atlas;
    protected Skeleton skeleton;
    protected AnimationState state;
    private SkeletonRenderer renderer;
    private float skeletonX, skeletonY;
    private boolean isPlay = true;
    private String animation;
    private float timeScale;
    private RendererGroup content = new RendererGroup();
    private boolean isUseAssetsManager = false;
    private String atlasFilePath;

    public SpineActor(Class<? extends Screen> screen) {}

    //region builders
    public static SpineActor create(String path, String id, Class<? extends Screen> screen) {
        return SpineActor.create(path, id, 1f, screen);
    }

    public static SpineActor create(String path, String id, float scale, Class<? extends Screen> screen) {
        if (!path.endsWith("/")){
            path = path + "/";
        }
        SkeletonXmlParser xmlParser = new SkeletonXmlParser(id + ".atlas", id + ".json");
        xmlParser.setUseDefaultAnimation(true);
        xmlParser.setUseJsonSize(true);
        return new SpineActor(path, xmlParser, scale, screen);
    }

    public static SpineActor createLazy(String path, String id, Class<? extends Screen> screen) {
        if (!path.endsWith("/")){
            path = path + "/";
        }
        SkeletonXmlParser xmlParser = new SkeletonXmlParser(id + ".atlas", id + ".json");
        xmlParser.setUseDefaultAnimation(true);
        xmlParser.setUseJsonSize(true);
//        return new LazyInitSpineactor(path, xmlParser, 1f, screen);
        return new SpineActor(path, xmlParser, 1f, screen);
    }
    //endregion


    //region initialization
    @Deprecated
    public SpineActor(String path) {
        this(path, 1f);
    }

    @Deprecated
    public SpineActor(String path, float scale) {
        this(path, new SkeletonXmlParser(path), scale, null);
    }

    public SpineActor(String path, SkeletonXmlParser xmlConfig, Class<? extends Screen> screen) {
        this(path, xmlConfig, 1f, screen);
    }

    public SpineActor(String path, SkeletonXmlParser xmlConfig, float scale, Class<? extends Screen> screen) {
       init(path, xmlConfig, scale);
    }

    public SpineActor(SpineActor spineActor) {
        init(spineActor);
    }

    protected SpineActor() {}

    protected void init(String path, SkeletonXmlParser xmlConfig, float scale) {
        this.id = path;
        this.xmlConfig = xmlConfig;
        createAtlas(path, xmlConfig);
        SkeletonJson json = createJson();
        SkeletonData skeletonData = createSkeletonData(path, xmlConfig, scale, json);
        createRenderer();
        applySizeAndPosition(xmlConfig, skeletonData);
        if (xmlConfig.isUseDefaultAnimation()) {
            animation =  skeletonData.getAnimations().get(0).getName();
        } else {
            animation = xmlConfig.getAnimation();
        }
        timeScale = xmlConfig.getTimeScale();
        skeleton = new Skeleton(skeletonData);
        createAnimationState();
        this.addActor(content);
        sizeChanged();
    }

    protected void init(SpineActor spineActor) {
        this.id = spineActor.id;
        this.xmlConfig = spineActor.xmlConfig;
        this.atlas = spineActor.atlas;
        this.skeleton = new Skeleton(spineActor.skeleton);
        SkeletonData skeletonData = skeleton.getData();
        this.renderer = spineActor.renderer;
        applySizeAndPosition(xmlConfig, skeletonData);
        if (xmlConfig.isUseDefaultAnimation()) {
            animation =  skeletonData.getAnimations().get(0).getName();
        } else {
            animation = xmlConfig.getAnimation();
        }
        timeScale = xmlConfig.getTimeScale();
        createAnimationState();
        state.clearTracks();
        this.addActor(content);
        sizeChanged();
    }

    private void createAtlas(String path, SkeletonXmlParser xmlConfig){
        isUseAssetsManager = xmlConfig.isUseAssetsManager();
        atlasFilePath = path + xmlConfig.getAtlas();
        //System.out.println("Spine loading " + path + id);
        atlas = Env.assets.get(atlasFilePath);
        if (atlas == null) {
            Gdx.app.error("SpineActor", "Error with loading spine actor " + id);
        }
    }

    private SkeletonJson createJson() {
        return new SkeletonJson(atlas);
    }

    private SkeletonData createSkeletonData(String path, SkeletonXmlParser xmlConfig, float scale, SkeletonJson json) {
        FileHandle file = Gdx.files.internal(path + xmlConfig.getSkeleton());
        json.setScale(xmlConfig.getScale() * scale);
        return json.readSkeletonData(file);
    }

    private void applySizeAndPosition(SkeletonXmlParser xmlConfig, SkeletonData skeletonData) {
        if (xmlConfig.isUseJsonSize()) {
            content.setSize(skeletonData.getWidth(), skeletonData.getHeight());
        } else {
            content.setSize(xmlConfig.getWidth(), xmlConfig.getHeight());
        }
        this.setSize(content.getWidth(), content.getHeight());
        skeletonX = 0;
        skeletonY = 0;
    }

    private void createRenderer() {
        renderer = new SkeletonRenderer();
        renderer.setPremultipliedAlpha(true);
    }

    private void createAnimationState() {
        AnimationStateData stateData = new AnimationStateData(skeleton.getData());
        state = new AnimationState(stateData);
        state.setAnimation(0, animation, true);
        state.setTimeScale(timeScale);
    }
    //endregion


    //region configure
    public String getCurrentAnimation(int trackIndex) {
        AnimationState.TrackEntry track = state.getCurrent(trackIndex);
        if (track != null) {
            Animation currentAnimation = track.getAnimation();
            if (currentAnimation != null) {
                return currentAnimation.getName();
            }
        }
        return null;
    }

    public float getAnimationDuration(String name) {
        Array<Animation> animations = state.getData().getSkeletonData().getAnimations();
        for (Animation animation : animations) {
            if (name != null && name.equals(animation.getName())) {
                return animation.getDuration();
            }
        }
        return 0f;
    }

    public void pauseTrack(int trackIndex) {
        AnimationState.TrackEntry track = state.getCurrent(trackIndex);
        if (track != null) {
            track.setTimeScale(0f);
        }
    }
    public void resumeTrack(int trackIndex) {
        AnimationState.TrackEntry track = state.getCurrent(trackIndex);
        if (track != null) {
            track.setTimeScale(1f);
        }
    }

    public void setTrackAlpha(int trackIndex, float alpha) {
        AnimationState.TrackEntry track = state.getCurrent(trackIndex);
        if (track != null) {
            track.setAlpha(alpha);
        }
    }

    public float getTrackAlpha(int trackIndex) {
        AnimationState.TrackEntry track = state.getCurrent(trackIndex);
        if (track != null) {
            return track.getAlpha();
        }
        return -1.0f;
    }

    public void setAnimation(String animationId){
        setAnimation(0, animationId, true);
    }

    public void setAnimation(int trackIndex, String animationId, boolean loop) {
        state.setAnimation(trackIndex, animationId, loop);
    }

    public void addAnimation(int trackIndex, String animationId, boolean loop, float delay) {
        state.addAnimation(trackIndex, animationId, loop, delay);
    }

    public void setEmptyAnimation(int trackIndex, float mixDuration) {
        state.setEmptyAnimation(trackIndex, mixDuration);
    }

    public void addEmptyAnimation(int trackIndex, float mixDuration, float delay) {
        state.addEmptyAnimation(trackIndex, mixDuration, delay);
    }


    public void setMix(String fromName, String toName, float duration) {
        state.getData().setMix(fromName, toName, duration);
    }

    public void setMix(int trackIndex, float duration) {
        AnimationState.TrackEntry track = state.getCurrent(trackIndex);
        if (track != null) {
            track.setMixDuration(duration);
        }
    }

    public void setTimeScale(float timeScale){
        state.setTimeScale(timeScale);
    }

    public void setSkin(String skinName) {
        Skin skin = skeleton.getData().findSkin(skinName);
        if (skin != null) {
            skeleton.setSkin(skin);
        } else {
            Gdx.app.error("SkeletonAnimationActor", "skin='" + skinName + "' not found in spine='" + id + "'");
        }
    }

    public void clearTracks() {
        state.clearTracks();
    }
    //endregion


    //region interface
    public void play() {
        isPlay = true;
    }

    public void pause() {
        isPlay = false;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        updateDelta(delta);
    }

    protected void updateDelta(float delta) {
        if (isPlay) {
            state.update(delta);
            skeleton.update(delta);
        }
    }

    @Override
    public void dispose() {
        atlas.dispose();
        atlas = null;
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        content.setScaleX(this.getWidth() / content.getWidth());
        content.setScaleY(this.getHeight() / content.getHeight());
    }

    //endregion

    private class RendererGroup extends Group {

        @Override
        protected void drawChildren(Batch batch, float parentAlpha) {
            int src = batch.getBlendSrcFunc();
            int dst = batch.getBlendDstFunc();
            int srcA = batch.getBlendSrcFuncAlpha();
            int dstA = batch.getBlendDstFuncAlpha();
            super.drawChildren(batch, parentAlpha);
            skeleton.setPosition(skeletonX + this.getX(), skeletonY + this.getY());
            state.apply(skeleton);
            skeleton.updateWorldTransform();
            skeleton.getColor().a = parentAlpha;
            renderer.draw((PolygonSpriteBatch) batch, skeleton);
            batch.setBlendFunctionSeparate(src, dst, srcA, dstA);
        }

//        @Override
//        public void draw(Batch batch, float parentAlpha) {
//            super.draw(batch, parentAlpha);
//            skeleton.setPosition(skeletonX + this.getX(), skeletonY + this.getY());
//            state.apply(skeleton);
//            if (isMeshInclude) {
//                skeleton.updateWorldTransform();
//            }
//            skeleton.getColor().a = parentAlpha;
//            renderer.draw((PolygonSpriteBatch) batch, skeleton);
//        }
    }
}
