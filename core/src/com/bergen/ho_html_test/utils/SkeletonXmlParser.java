package com.bergen.ho_html_test.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by anbu on 03.03.18.
 */

public class SkeletonXmlParser {
    private String atlas;
    private String skeleton;
    private String animation;
    private float timeScale = 1f;
    private float scale = 1f;
    private float offsetX = 0;
    private float offsetY = 0;
    private float width = 0;
    private float height = 0;
    private boolean isUseJsonSize = false;
    private boolean isUseDefaultAnimation = false;
    private boolean isUseAssetsManager = false;

    public SkeletonXmlParser(String path) {
        if (!path.endsWith("/")){
            path = path + "/";
        }
        XmlReader reader = new XmlReader();
        FileHandle file = Gdx.files.internal(path + "animation.xml");
        XmlReader.Element root = reader.parse(file);
        parseRoot(root);
    }

    private void parseRoot(XmlReader.Element root) {
        atlas = root.getAttribute("atlas");
        skeleton = root.getAttribute("skeleton");
        animation = root.getAttribute("animation");
        timeScale = root.getFloatAttribute("time_scale", 1f);
        width = root.getFloatAttribute("width", 0f);
        height = root.getFloatAttribute("height", 0f);
        scale = root.getFloatAttribute("scale", 1f);
        offsetX = root.getFloatAttribute("offset_x", 0f);
        offsetY = root.getFloatAttribute("offset_y", 0f);
    }

    public SkeletonXmlParser(String atlas, String skeleton) {
        this.atlas = atlas;
        this.skeleton = skeleton;
    }

    public String getAtlas() {
        return atlas;
    }

    public String getSkeleton() {
        return skeleton;
    }

    public String getAnimation() {
        return animation;
    }

    public float getTimeScale() {
        return timeScale;
    }

    public float getScale() {
        return scale;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public boolean isUseJsonSize() {
        return isUseJsonSize;
    }

    public void setUseJsonSize(boolean useJsonSize) {
        isUseJsonSize = useJsonSize;
    }

    public boolean isUseDefaultAnimation() {
        return isUseDefaultAnimation;
    }

    public void setUseDefaultAnimation(boolean useDefaultAnimation) {
        isUseDefaultAnimation = useDefaultAnimation;
    }

    public boolean isUseAssetsManager() {
        return isUseAssetsManager;
    }

    public void setUseAssetsManager(boolean useAssetsManager) {
        isUseAssetsManager = useAssetsManager;
    }
}
