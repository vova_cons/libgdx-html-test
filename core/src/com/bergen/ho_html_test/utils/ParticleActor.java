package com.bergen.ho_html_test.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ParticleActor extends Actor {
	public ParticleEffect effect;
	public boolean isActive;
	
	public ParticleActor(ParticleEffect effect) {
		this.effect = effect;
		isActive = true;
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		setParticlePosition(getX(), getY());
		effect.update(delta);
		for (ParticleEmitter emitter : effect.getEmitters()) {
			emitter.getTransparency().setHigh(getColor().a);
		}
	}

	public void setActive(boolean active){
		isActive = active;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		effect.draw(batch);
	}
	
	public ParticleEffect getParticleEffect()
	{
		return effect;
	}
	
	public void start()
	{
		effect.start();
	}
	
	@Override
	public void setPosition(float x, float y) {
		super.setPosition(x, y);
		setParticlePosition(x, y);
	}

	protected void setParticlePosition(float x, float y) {
		effect.setPosition(x, y);
	}

	@Override
	public void setColor(Color color){
		for (int i = 0; i < effect.getEmitters().size; i++) {
			effect.getEmitters().get(i).getSprites().first().setColor(color);
//			effect.getEmitters().get(i).getSprite().setColor(color);
		}
		super.setColor(color);
	}

	@Override
	public void setScale(float scaleXY){
		super.setScale(scaleXY);
		for(int i = 0; i < effect.getEmitters().size; i++) {
			Sprite particleSprite = effect.getEmitters().get(i).getSprites().first();
			particleSprite.setSize(particleSprite.getWidth() * scaleXY, particleSprite.getHeight() * scaleXY);
		}
	}


}
