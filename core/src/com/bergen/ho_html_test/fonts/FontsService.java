package com.bergen.ho_html_test.fonts;

import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Created by anbu on 23.01.19.
 * Старый класс созданный алексеем.
 */
public interface FontsService {
    BitmapFont getFont(Size size);
    BitmapFont getFont(Purpose purpose, Size sizeName);
    float getTargetScale(Size fontName);
    boolean isOptimizationEnabled();
    void dispose();

    enum Purpose {
        Default,
        HandWritten,
        WindowTitle
    }

    enum Size {
        Title(95), Subtitle(85), H0(72), H1(60), H2(50), H3(40);

        int size;

        Size(int size) {
            this.size = size;
        }
    }
}
