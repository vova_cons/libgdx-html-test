package com.bergen.ho_html_test.fonts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by anbu on 23.01.19.
 **/
public class FontsServiceV1 implements FontsService {
    private static final String chars = "1234567890" + "/!@#$%^&*()\\\"'’…,.{}[];:?-+=<>«»_`~–" +
            "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm" +
            "йцукенгшщзхъфывапролджэёячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЁЯЧСМИТЬБЮ";
    private ObjectMap<Purpose, ObjectMap<Size, BitmapFont>> data = new ObjectMap<Purpose, ObjectMap<Size, BitmapFont>>();
    private final int STOCK_FONT_SIZE = 24;
    private final FontsService.Size STOCK_FONT_SIZE_NAME = FontsService.Size.H2;
    private boolean isOptimizationEnabled = false;

    //region initialization
    public FontsServiceV1() {
        data.put(Purpose.Default, generateTypeface("source_sans_pro_bold.ttf"));
        data.put(Purpose.HandWritten, generateTypeface("neucha.ttf"));
        data.put(Purpose.WindowTitle, generateTypeface("pattaya_regular.ttf"));
    }

    private ObjectMap<Size, BitmapFont> generateTypeface(String fontName){
        ObjectMap<Size, BitmapFont> out = new ObjectMap<Size, BitmapFont>();
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/" + fontName));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.characters = chars;
        parameter.magFilter = Texture.TextureFilter.Linear;
        parameter.minFilter = Texture.TextureFilter.Linear;
        if(isOptimizationEnabled()) {
            parameter.size = STOCK_FONT_SIZE;
            out.put(STOCK_FONT_SIZE_NAME, generator.generateFont(parameter));
        } else {
            for(Size size : Size.values()) {
                parameter.size = size.size;
                out.put(size, generator.generateFont(parameter));
            }
        }
        generator.dispose();
        return out;
    }
    //endregion


    //region interface
    @Override
    public BitmapFont getFont(FontsService.Size size){
        return getFont(FontsService.Purpose.Default, size);
    }

    @Override
    public BitmapFont getFont(FontsService.Purpose purpose, FontsService.Size sizeName) {
        FontsService.Size size = isOptimizationEnabled() ? STOCK_FONT_SIZE_NAME : sizeName;
        return data.get(purpose).get(size);
    }

    @Override
    public float getTargetScale(FontsService.Size fontName) {
        return (float)fontName.size / (float) STOCK_FONT_SIZE;
    }

    @Override
    public boolean isOptimizationEnabled() {
        return isOptimizationEnabled;
    }

    @Override
    public void dispose() {
        if (data != null) {
            for(ObjectMap.Entry<Purpose, ObjectMap<Size, BitmapFont>> entry : data.entries()) {
                for(ObjectMap.Entry<Size, BitmapFont> entry1 : entry.value.entries()) {
                    entry1.value.dispose();
                }
            }
            data.clear();
            data = null;
        }
    }
    //endregion
}
