package com.bergen.ho_html_test.tests;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.bergen.ho_html_test.BaseScreen;
import com.bergen.ho_html_test.Env;

/**
 * Created by anbu on 23.10.19.
 **/
public class TestView extends Group {
    protected Label label;

    public void createLabel(String text) {
        BitmapFont defaultH1Font = Env.assets.get("fonts/source_sans_pro_bold_95.fnt");
        Label.LabelStyle style = new Label.LabelStyle(defaultH1Font, Color.RED);
        label = new Label(text, style);
        label.setSize(BaseScreen.WIDTH, BaseScreen.HEIGHT);
        label.setAlignment(Align.center);
        label.setWrap(true);
        this.addActor(label);
        label.setTouchable(Touchable.disabled);
    }

    public void show() {
        this.setVisible(true);
    }

    public void hide() {
        this.setVisible(false);
    }
}
