package com.bergen.ho_html_test.tests;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.I18NBundle;
import com.bergen.ho_html_test.BaseScreen;
import com.bergen.ho_html_test.Env;

import java.util.Locale;

/**
 * Created by anbu on 23.10.19.
 **/
public class LocalizationTestView extends TestView {
    private boolean isRus = false;
    private FileHandle bundleFile;

    public LocalizationTestView() {
        Image bg = new Image(Env.assets.get("bg.jpg", Texture.class));
        bg.setPosition(BaseScreen.WIDTH / 2f, BaseScreen.HEIGHT / 2f, Align.center);
        this.addActor(bg);
        createLabel("Localization test");
        bundleFile = Gdx.files.internal("localization/store");
        updateLabel();
        bg.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                updateLabel();
            }
        });
    }

    private void updateLabel() {
        I18NBundle bundle = I18NBundle.createBundle(bundleFile, createLocale(isRus), "UTF-8");
        String text = bundle.get("text");
        String language = bundle.get("language");
        label.setText(language + "\n" + "isRus=" + isRus + "\n" + text);
        Gdx.app.log("TestScreen", "Change label text to isRus=" + isRus);
        isRus = !isRus;
    }

    private Locale createLocale(boolean isRus) {
        if (isRus) { //refactor this shit
            return new Locale("ru", "RU");
        }
        return new Locale("en", "US");
    }
}
