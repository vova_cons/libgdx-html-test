package com.bergen.ho_html_test.tests;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.XmlReader;

/**
 * Created by anbu on 24.10.19.
 **/
public class LazyLoadingAssetsTestView extends TestView {
    private Group content;
    private AssetManager assetManager = new AssetManager();
    private boolean isLoadingStart = false;

    public LazyLoadingAssetsTestView() {
        content = new Group();
        this.addActor(content);
        createLabel("Lazy loading assets test view");
    }

    @Override
    public void show() {
        super.show();
        assetManager.load("textures/bg.jpg", Texture.class);
        assetManager.load("textures/pack.atlas", TextureAtlas.class);
//        assetManager.load("textures/location.xml", FileHandle.class);
        isLoadingStart = true;
    }

    @Override
    public void hide() {
        super.hide();
        content.clearChildren();
        isLoadingStart = false;
        assetManager.clear();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (isLoadingStart) {
            boolean isFinish = assetManager.update();
            if (isFinish) {
                onFinishLoading();
            }
        }
    }

    private void onFinishLoading() {
        isLoadingStart = false;
        Texture bgTexture = assetManager.get("textures/bg.jpg");
        Image bg = new Image(bgTexture);
        content.addActor(bg);
        TextureAtlas atlas = assetManager.get("textures/pack.atlas");
        XmlReader reader = new XmlReader();
        //XmlReader.Element root = reader.parse(assetManager.get("textures/location.xml", FileHandle.class));
        XmlReader.Element root = reader.parse(Gdx.files.internal("textures/location.xml"));
        for(XmlReader.Element goodElement : root.getChildrenByName("good")) {
            String name = goodElement.getAttribute("name");
            String variation = goodElement.getAttribute("variation");
            int x = goodElement.getIntAttribute("x");
            int y = goodElement.getIntAttribute("y");
            int w = goodElement.getIntAttribute("w");
            int h = goodElement.getIntAttribute("h");
            String goodPath = "goods/" + name + "_" + variation;
            TextureAtlas.AtlasRegion texture = atlas.findRegion(goodPath);
            final Image goodImage = new Image(texture);
            goodImage.setSize(w, h);
            goodImage.setPosition(x, y);
            content.addActor(goodImage);
            goodImage.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    goodImage.setVisible(false);
                }
            });
        }
    }
}
