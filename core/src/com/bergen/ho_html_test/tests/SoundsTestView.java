package com.bergen.ho_html_test.tests;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.bergen.ho_html_test.BaseScreen;
import com.bergen.ho_html_test.Env;

/**
 * Created by anbu on 23.10.19.
 **/
public class SoundsTestView extends TestView {
    private final Music music;

    public SoundsTestView() {
        Image bg = new Image(Env.assets.get("bg.jpg", Texture.class));
        bg.setPosition(BaseScreen.WIDTH / 2f, BaseScreen.HEIGHT / 2f, Align.center);
        this.addActor(bg);
        music = Env.assets.get("sounds/store_music.mp3");
        music.setLooping(true);
        createLabel("Music test view");
    }

    @Override
    public void show() {
        super.show();
        music.play();
    }

    @Override
    public void hide() {
        super.hide();
        music.stop();
    }
}
