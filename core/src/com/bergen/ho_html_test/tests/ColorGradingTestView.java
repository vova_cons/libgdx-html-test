package com.bergen.ho_html_test.tests;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.bergen.ho_html_test.BaseScreen;
import com.bergen.ho_html_test.Env;
import com.bergen.ho_html_test.utils.ColorGrading;

/**
 * Created by anbu on 23.10.19.
 **/
public class ColorGradingTestView extends TestView {
    private final ColorGrading colorGrading;
    private float progress = 0f;

    public ColorGradingTestView() {
        colorGrading = new ColorGrading();
        Image bg = new Image(Env.assets.get("bg.jpg", Texture.class));
        bg.setPosition(BaseScreen.WIDTH / 2f, BaseScreen.HEIGHT / 2f, Align.center);
        colorGrading.addActor(bg);
        colorGrading.setPalette(Env.assets.get("color_grade/base.png", Texture.class));
        colorGrading.setPaletteNight(Env.assets.get("color_grade/night.png", Texture.class));
        colorGrading.setProgress(progress);
        this.addActor(colorGrading);
        createLabel("Color grading shader test view\nUse arrow up/down for change color grading progress");
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (isVisible()) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
                progress -= 0.1f;
                if (progress < 0) {
                    progress = 0f;
                }
                colorGrading.setProgress(progress);
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
                progress += 0.1f;
                if (progress > 1f) {
                    progress = 1f;
                }
                colorGrading.setProgress(progress);
            }
        }
    }
}
