package com.bergen.ho_html_test.tests;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.bergen.ho_html_test.BaseScreen;
import com.bergen.ho_html_test.Env;
import com.bergen.ho_html_test.utils.CropShaderedGroup;

/**
 * Created by anbu on 23.10.19.
 **/
public class CropShaderTestView extends TestView {
    public CropShaderTestView() {
        CropShaderedGroup cropShaderedGroup = new CropShaderedGroup((int)BaseScreen.WIDTH, (int)BaseScreen.HEIGHT);
        Image bg = new Image(Env.assets.get("bg.jpg", Texture.class));
        bg.setPosition(BaseScreen.WIDTH / 2f, BaseScreen.HEIGHT / 2f, Align.center);
        cropShaderedGroup.setOriginalActor(bg);
        Image mask = new Image(Env.assets.get("mask.png", Texture.class));
        mask.setPosition(BaseScreen.WIDTH / 2f, BaseScreen.HEIGHT / 2f, Align.center);
        cropShaderedGroup.setMaskActor(mask);
        this.addActor(cropShaderedGroup);
        createLabel("Crop shader test view");
    }
}
