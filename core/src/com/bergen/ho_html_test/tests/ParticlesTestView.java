package com.bergen.ho_html_test.tests;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.bergen.ho_html_test.BaseScreen;
import com.bergen.ho_html_test.Env;
import com.bergen.ho_html_test.utils.ParticleActor;

/**
 * Created by anbu on 23.10.19.
 **/
public class ParticlesTestView extends TestView {
    public ParticlesTestView() {
        Image bg = new Image(Env.assets.get("bg.jpg", Texture.class));
        bg.setPosition(BaseScreen.WIDTH / 2f, BaseScreen.HEIGHT / 2f, Align.center);
        this.addActor(bg);
        ParticleEffect particleEffect = new ParticleEffect();
        particleEffect.load(Gdx.files.internal("particles/bokeh.txt"), Gdx.files.internal("particles/"));
        ParticleActor particleActor = new ParticleActor(particleEffect);
        particleActor.setPosition(BaseScreen.WIDTH/2f, BaseScreen.HEIGHT/2f, Align.center);
        this.addActor(particleActor);
        createLabel("Particles test view");
    }
}
