package com.bergen.ho_html_test.tests;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.bergen.ho_html_test.BaseScreen;
import com.bergen.ho_html_test.Env;
import com.bergen.ho_html_test.TestScreen;
import com.bergen.ho_html_test.utils.SpineActor;

/**
 * Created by anbu on 23.10.19.
 **/
public class SpinTestView extends TestView {
    public SpinTestView() {
        Image bg = new Image(Env.assets.get("bg.jpg", Texture.class));
        bg.setPosition(BaseScreen.WIDTH / 2f, BaseScreen.HEIGHT / 2f, Align.center);
        this.addActor(bg);
        SpineActor spineActor = SpineActor.create("animations/stash/", "stash", TestScreen.class);
        spineActor.setSize(736,736);
        spineActor.setPosition(BaseScreen.WIDTH / 2f, BaseScreen.HEIGHT / 2f, Align.center);
        this.addActor(spineActor);
        spineActor.setAnimation(0, "idle", true);
        spineActor.play();
        createLabel("Spine actor test view");
    }
}
