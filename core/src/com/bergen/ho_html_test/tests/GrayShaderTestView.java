package com.bergen.ho_html_test.tests;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.bergen.ho_html_test.BaseScreen;
import com.bergen.ho_html_test.Env;
import com.bergen.ho_html_test.utils.ShaderedGroup;

/**
 * Created by anbu on 23.10.19.
 **/
public class GrayShaderTestView extends TestView {
    private final ShaderedGroup content;

    public GrayShaderTestView() {
        ShaderProgram grayShader = new ShaderProgram(Gdx.files.internal("shaders/gray.vert"), Gdx.files.internal("shaders/gray.frag"));
        content = new ShaderedGroup(grayShader);
        Image bg = new Image(Env.assets.get("bg.jpg", Texture.class));
        bg.setPosition(BaseScreen.WIDTH / 2f, BaseScreen.HEIGHT / 2f, Align.center);
        content.addActor(bg);
        this.addActor(content);
        content.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                content.setShaderEnable(!content.isShaderEnabled());
            }
        });
        createLabel("Gray shader test view\nClick anywhere for enable/disable shader");
    }
}
