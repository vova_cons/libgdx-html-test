package com.bergen.ho_html_test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

/**
 * Created by anbu on 24.10.19.
 **/
public class LoadingScreen extends BaseScreen {
    @Override
    public void start() {
        Image image = new Image(new Texture(Gdx.files.internal("preload/bg_default.jpg")));
        image.setPosition(WIDTH/2f, HEIGHT/2f, Align.center);
        this.addActor(image);

        Env.assets.load("animations/spine_firework/firework.atlas", TextureAtlas.class);
        Env.assets.load("animations/stash/stash.atlas", TextureAtlas.class);
        Env.assets.load("color_grade/base.png", Texture.class);
        Env.assets.load("color_grade/night.png", Texture.class);
        Env.assets.load("fonts/source_sans_pro_bold_95.fnt", BitmapFont.class);
        Env.assets.load("sounds/store_music.mp3", Music.class);
        Env.assets.load("background1.jpg", Texture.class);
        Env.assets.load("bg.jpg", Texture.class);
        Env.assets.load("bg_default.jpg", Texture.class);
        Env.assets.load("mask.png", Texture.class);
    }

    @Override
    public void update(float delta) {
        boolean isLoaded = Env.assets.update();
        if (isLoaded) {
            changeScreen();
        }
    }

    private void changeScreen() {
        Env.game.changeScreen(new TestScreen());
    }
}
