#ifdef GL_ES
precision mediump float;
precision mediump int;
#else
#define highp;
#endif

uniform sampler2D u_texture;
uniform float multiplier;

varying vec4 v_color;
varying vec2 v_texCoord;


void main() {
    vec4 texColor = texture2D(u_texture, v_texCoord);
    float sum = texColor.r + texColor.g + texColor.b;
    sum = sum / 3.0;
    vec3 rgb = vec3(sum);
    gl_FragColor = vec4(rgb, texColor.a * v_color.a);
}