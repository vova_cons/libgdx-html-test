#ifdef GL_ES
precision mediump float;
precision mediump int;
#else
#define highp;
#endif

varying vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform sampler2D u_palette;
uniform sampler2D u_paletteNight;
uniform float u_lutSize;
uniform float u_progress;


vec4 sampleAs3DTexture(sampler2D palette_texture, vec3 inputColor, float size) {
    float sliceSize = 1.0 / size;
    float slicePixelSize = sliceSize / size;
    float sliceInnerSize = slicePixelSize * (size - 1.0);
    float bSlice0 = min(floor(inputColor.b * (size - 1.0)), size - 1.0);
    float bSlice1 = min(bSlice0 + 1.0, size - 1.0);
    float rOffset = slicePixelSize * 0.5 + inputColor.r * sliceInnerSize;
    float s0 = rOffset + bSlice0 * sliceSize;
    float s1 = rOffset + bSlice1 * sliceSize;
    float g = sliceSize * 0.5 + inputColor.g * sliceSize * (size - 1.0);
    vec4 slice0Color = texture2D(palette_texture, vec2(s0, g));
    vec4 slice1Color = texture2D(palette_texture, vec2(s1, g));
    float zOffset = mod(inputColor.b * (size - 1.0), 1.0);
    return mix(slice0Color, slice1Color, zOffset);
}

void main() {
    vec3 inputColor = texture2D(u_texture, v_texCoords).rgb;
    vec4 nightColor = sampleAs3DTexture(u_paletteNight, inputColor, u_lutSize);
    vec4 normalColor = sampleAs3DTexture(u_palette, inputColor, u_lutSize);
    gl_FragColor = mix(normalColor, nightColor, u_progress);
}