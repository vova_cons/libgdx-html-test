#ifdef GL_ES
precision mediump float;
#endif

varying vec2 v_texCoord;
varying vec4 v_color;

uniform sampler2D u_texture;
uniform sampler2D u_mask;

void main() {
    float maskColor = texture2D(u_mask, v_texCoord).r;
    vec4 texColor = texture2D(u_texture, v_texCoord);
    if (maskColor < 0.25) {
        gl_FragColor = vec4(texColor.rgb, 0.0);
    } else {
        gl_FragColor = vec4(texColor.rgb, texColor.a);
    }
}