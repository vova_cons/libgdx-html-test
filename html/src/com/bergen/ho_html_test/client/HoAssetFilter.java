package com.bergen.ho_html_test.client;

import com.badlogic.gdx.backends.gwt.preloader.AssetFilter;

/**
 * Created by anbu on 24.10.19.
 **/
public class HoAssetFilter implements AssetFilter {
    private String extension (String file) {
        String name = file;
        int dotIndex = name.lastIndexOf('.');
        if (dotIndex == -1) return "";
        return name.substring(dotIndex + 1);
    }

    @Override
    public boolean accept (String file, boolean isDirectory) {
        String normFile = file.replace('\\', '/');
        if (normFile.contains("/.")) {
            return false;
        }
        if (normFile.contains("/_")) {
            return false;
        }
        if (isDirectory && file.endsWith(".svn")) {
            return false;
        }
        return true;
    }

    //@Override
    public boolean preload(String file) {
        if (file.endsWith(".xml") || file.endsWith(".json") || file.endsWith(".frag") || file.endsWith(".vert")) {
            return true;
        }
        if (file.startsWith("animations/")) {
            return false;
        }
        if (file.startsWith("color_grade/")) {
            return false;
        }
        if (file.startsWith("fonts/")) {
            return false;
        }
        if (file.startsWith("localization/")) {
            return true;
        }
        if (file.startsWith("particles/")) {
            return true;
        }
        if (file.startsWith("preload/")) {
            return true;
        }
        if (file.startsWith("shaders/")) {
            return true;
        }
        if (file.startsWith("sounds/")) {
            return false;
        }
        if (file.startsWith("textures/")) {
            return false;
        }
        if (file.endsWith(".png") || file.endsWith(".jpg") || file.endsWith(".mp3") || file.endsWith(".atlas")){
            return false;
        }
        return false;
    }

    @Override
    public AssetType getType (String file) {
        String extension = extension(file).toLowerCase();
        if (isImage(extension)) return AssetType.Image;
        if (isAudio(extension)) return AssetType.Audio;
        if (isText(extension)) return AssetType.Text;
        return AssetType.Binary;
    }

    private boolean isImage (String extension) {
        return extension.equals("jpg") || extension.equals("jpeg") || extension.equals("png") || extension.equals("bmp") || extension.equals("gif");
    }

    private boolean isText (String extension) {
        return extension.equals("json") || extension.equals("xml") || extension.equals("txt") || extension.equals("glsl")
                || extension.equals("fnt") || extension.equals("pack") || extension.equals("obj") || extension.equals("atlas")
                || extension.equals("g3dj") || extension.equals(".vert") || extension.equals(".frag");
    }

    private boolean isAudio (String extension) {
        return extension.equals("mp3") || extension.equals("ogg") || extension.equals("wav");
    }

    @Override
    public String getBundleName (String file) {
        return "assets";
    }
}
